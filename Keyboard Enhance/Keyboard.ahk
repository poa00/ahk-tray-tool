;#NoEnv
;SendMode Input
;SetWorkingDir %A_ScriptDir%

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;AutoHotKey Startup Essentials

;Load Icon
#SingleInstance ignore
Menu, Tray, Tip, Keyboard Enhance
Menu, Tray, Icon, imageres_5206.ico,,1
return

;Toggle suspend-state icons
;^!CTRLBREAK::
^!Home::
Suspend, Toggle
if A_IsSuspended=1
    Menu, Tray, Icon, imageres_5206v2.ico,,1
else
    Menu, Tray, Icon, imageres_5206.ico,,1
;Menu, Tray, Icon, DDORes_3015.ico,,1
return	

;To use Backspace as "Higher Directory" in windows file explorer.
#IfWinActive, ahk_class CabinetWClass
Backspace::
   ControlGet renamestatus,Visible,,Edit1,A
   ControlGetFocus focussed, A
   if(renamestatus!=1&&(focussed="DirectUIHWND3"||focussed=SysTreeView321))
   {
    SendInput {Alt Down}{Up}{Alt Up}
  }else{
      Send {Backspace}
  }
#IfWinActive
return

;Help for AutoHotKey
^!F1::
	KeyWait F1
	run %ProgramFiles%\AutoHotkey\AutoHotKey.chm
return

;Edit AutoHotKey script
^!F2::
	KeyWait F2
	;KeyWait ALT
	;KeyWait CTRL
	Edit
	;;run "C:\Program Files (x86)\Notepad++\notepad++.exe", %a_scriptfullpath%
return

;Reload AutoHotKey script
^!F5::
	KeyWait F5
	;KeyWait ALT
	;KeyWait CTRL
	Reload
return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Media settings

;Load last playlist on Windows Media Player
^!l::
	KeyWait l
	;KeyWait ALT
	;KeyWait CTRL
	run C:\Program Files (x86)\Windows Media Player\wmplayer.exe /prefetch:11 /Query:3;3;6;Resume previous list;29655;-1;;;;0;;;;1;;
return

;Previous Track
!Left::
	KeyWait Left
	;KeyWait ALT
	Send {Media_Prev}
return

;Pause/Play Track
!Down::
	KeyWait Down
	;KeyWait ALT
	Send {Media_Play_Pause}
return

;Next Track
!Right::
	KeyWait Right
	;KeyWait ALT
	Send {Media_Next}
return

;Stop Track
!Up::
	KeyWait Up
	;KeyWait ALT
	Send {Media_Stop}
return


;Volume up
APPSKEY & Up::
	;KeyWait Up
	;KeyWait APPSKEY
	Send {Volume_Up}
return

;Volume down and mute
APPSKEY & Down::
	GetKeyState, state, SHIFT
	if state = D
	{
		KeyWait Down
		Send {Volume_Mute}
	}
	else
	{
		Send {Volume_Down}
	}
return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;System power save add-ons

;Turn off monitor
#Space::
	KeyWait LWIN
	KeyWait Space
	SendMessage,0x112,0xF170,2,,Program Manager
	return

;Turn off monitor and lock system
#+l::
	KeyWait LWIN
	KeyWait Shift
	KeyWait l
	DllCall("LockWorkStation")
	SendMessage,0x112,0xF170,2,,Program Manager
return






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Common folders


^!m::
	KeyWait m
	;KeyWait ALT
	;KeyWait CTRL
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Music.library-ms
return


^!+m::
	KeyWait m
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Movies.library-ms
return


^!d::
	KeyWait d
	;KeyWait ALT
	;KeyWait CTRL
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Documents.library-ms
return


^!+d::
	KeyWait d
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Downloads.library-ms
return


^!p::
	KeyWait p
	;KeyWait ALT
	;KeyWait CTRL
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Programming.library-ms
return


^!+p::
	KeyWait p
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Pictures.library-ms
return

^!v::
	KeyWait v
	;KeyWait ALT
	;KeyWait CTRL
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Videos.library-ms
return


#+d::
	KeyWait d
	KeyWait SHIFT
	KeyWait LWIN
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Desktop.library-ms
return


#q::
	KeyWait q
	KeyWait LWIN
	run D:\Programming\Windows 8 Shortcuts
return


#+s::
	KeyWait s
	KeyWait SHIFT
	KeyWait LWIN
	run %appdata%\Microsoft\Windows\Start Menu\Programs\Startup
return



^!i::
	KeyWait i
	;KeyWait ALT
	;KeyWait CTRL
	run %userprofile%\AppData\Roaming\Microsoft\Windows\Libraries\Installations.library-ms
return

^!+/::
	KeyWait /
	run "D:\Documents\College\Academics\Semester 8"
return


^!/::
	KeyWait /
	run "D:\Documents\College"
return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;System Shortcuts


;Windows Update - Metro
^!u::
	KeyWait u
	;KeyWait ALT
	;KeyWait CTRL
	run control wuaucpl.cpl
return
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

;Programs and Features (uninstall)
^!+u::
	KeyWait u
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run control appwiz.cpl
return

;Open CCleaner
#z::
	KeyWait z
	KeyWait LWIN
	run ccleaner
return

;Open Defraggler
#+z::
	KeyWait z
	KeyWait SHIFT
	KeyWait LWIN
	run defraggler
return

;Open Connections
#+c::
	KeyWait c
	KeyWait SHIFT
	KeyWait LWIN
	run %windir%\explorer.exe shell:::{38A98528-6CBF-4CA9-8DC0-B1E1D10F7B1B}
return
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

;Camera
^!+c::
	KeyWait c
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run %windir%\explorer.exe shell:Appsfolder\Microsoft.MoCamera_cw5n1h2txyewy!Microsoft.Camera
return
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;Calculator
^!c::
	KeyWait c
	;KeyWait ALT
	;KeyWait CTRL
	run %windir%\system32\calc.exe
return


;Alarms - Metro
^!a::
	KeyWait a
	;KeyWait ALT
	;KeyWait CTRL
	run %windir%\explorer.exe shell:Appsfolder\Microsoft.WindowsAlarms_8wekyb3d8bbwe!App
return


;Skype - Desktop
^!s::
	KeyWait s
	;KeyWait ALT
	;KeyWait CTRL
	run C:\Program Files (x86)\Skype\Phone\Skype.exe
return
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
;Sound Propeties
^!+s::
	KeyWait s
	;KeyWait ALT
	;KeyWait CTRL
	run control mmsys.cpl sounds
return

;Command prompt
APPSKEY & c::
	KeyWait c
	;KeyWait APPSKEY
	run, cmd, %userprofile%
return

;LAN Settings
APPSKEY & l::
	KeyWait l
	;KeyWait APPSKEY
	runwait, inetcpl.cpl, 4 ,, Hide
	
return

;;Powershell
;APPSKEY & p::
;	KeyWait p
;	;KeyWait APPSKEY
;	run, powershell, %userprofile%
;return

APPSKEY & p::
	GetKeyState, state, SHIFT
	if state = D
	{
		KeyWait p
		;KeyWait SHIFT
		;KeyWait APPSKEY
		runwait, reg add "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyEnable /t REG_DWORD /d 00000000 /f ,, Hide
		MsgBox, 4096, Proxy Status, The current proxy settings have been disabled., 1.5
	}
	else
	{
		KeyWait p
		;KeyWait APPSKEY
		runwait, reg add "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyEnable /t REG_DWORD /d 00000001 /f ,, Hide
		MsgBox, 4096, Proxy Status, The current proxy settings have been enabled., 1.5	
	}
return


;Notepad
^!n::
	KeyWait n
	;KeyWait ALT
	;KeyWait CTRL
	run notepad.exe
return

^!+n::
	KeyWait n
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run C:\Program Files\Sublime Text 3\sublime_text.exe
return
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

APPSKEY & i::
	GetKeyState, state, SHIFT
	if state = D
	{
		KeyWait i
		;KeyWait SHIFT
		;KeyWait APPSKEY
		run http://172.16.16.16/24online/webpages/client.jsp?fromlogout=true
	}
	else
	{
		KeyWait i
		;KeyWait APPSKEY
		SetWorkingDir %A_WirkingDir%
		run, ping 172.16.16.16 -t
	}
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Others

^!g::
	KeyWait g
	;KeyWait ALT
	;KeyWait CTRL
	run https://inbox.google.com
return

^!+g::
	KeyWait g
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run https://mail.google.com
return

^!o::
	KeyWait o
	Run "C:\Program Files\Microsoft Office\root\Office16\OUTLOOK.EXE"

^!w::
	KeyWait w
	;KeyWait ALT
	;KeyWait CTRL
	;run https://web.whatsapp.com
	Run "C:\Users\Soorya Annadurai\AppData\Local\WhatsApp\Update.exe" --processStart WhatsApp.exe
return

^!+w::
	KeyWait w
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run https://www.wunderlist.com
return

^!b::
	KeyWait b
	;KeyWait ALT
	;KeyWait CTRL
	run D:\Programming\Windows 8 Shortcuts\Bluetooth File Transfer.exe
return
;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
^!h::
	KeyWait h
	;KeyWait ALT
	;KeyWait CTRL
	;run C:\Program Files (x86)\Hewlett-Packard\HP Wireless Hotspot\WirelessHotSpot.exe
	RunWait netsh wlan set hostednetwork mode=allow ssid=%A_ComputerName% key=blueSpot keyUsage=persistent,,Hide
	RunWait netsh wlan start hostednetwork ,, Hide
	MsgBox, 4096, Hotspot active, %A_ComputerName% is now broadcasting., 1.3	
return

^!+h::
	KeyWait h
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	RunWait netsh wlan stop hostednetwork ,, Hide
	MsgBox, 4096, Hotspot inactive, %A_ComputerName% has stopped broadcasting., 1.3
return

;Resetting the wireless hostednetwork status does not work. Manually enter these commands.
APPSKEY & h::
	GetKeyState, state, SHIFT
	if state = D
	{
		KeyWait APPSKEY
		KeyWait SHIFT
		KeyWait h
		RunWait net stop wlansvc & PAUSE
		RunWait reg delete hklm\system\currentcontrolset\services\wlansvc\parameters\hostednetworksettings /v hostednetworksettings & PAUSE
		RunWait net start wlansvc & PAUSE
	}
	else
	{
		;KeyWait APPSKEY
		KeyWait h
		;Run cmd /c echo Hotspot Status: & netsh wlan show hostednetwork & PAUSE
		MsgBox, 4096, Wireless Broadcast status, % RunWaitOne("netsh wlan show hostednetwork"), 2.5
	}
return

RunWaitOne(command) {
    ; WshShell object: http://msdn.microsoft.com/en-us/library/aew9yb99
    shell := ComObjCreate("WScript.Shell")
    ; Execute a single command via cmd.exe
    exec := shell.Exec(ComSpec " /C " command)
    ; Read and return the command's output
    return exec.StdOut.ReadAll()
}

;Recycle Bin
APPSKEY & r::
	KeyWait r
	Run ::{645FF040-5081-101B-9F08-00AA002F954E} 
return

;Playback devices
APPSKEY & s::
	Keywait s
	Run, mmsys.cpl
return

^!r::
	KeyWait r
	;KeyWait ALT
	;KeyWait CTRL
	run C:\Program Files (x86)\Unified Remote 3.0\RemoteServerWin.exe
	;run C:\Program Files (x86)\Unified Remote\RemoteServer.exe
return

^!+r::
	KeyWait r
	;KeyWait SHIFT
	;KeyWait ALT
	;KeyWait CTRL
	run C:\Program Files (x86)\WOMic\WOMicClient.exe
return


#`::
	KeyWait LWIN
	run D:\Programming\Jarvis\Jarvis\Jarvis\bin\Release\Jarvis.exe
	;MsgBox, 4096, Jarvis, Jarvis will be online soon., 1.5
return